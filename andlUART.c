/*
 * ========================================
 * [--------------andlUART.c--------------]
 * ========================================
 * This source file has definitions of functions which can initialize and use UART.
 * It can be used for compile static library or direct include to Your project.
 *
 * Created by andl on 30.03.2019
 */

// ----------------INCLUDES----------------
#include "andlUART.h"

// ---------------FUNCTIONS----------------

#ifdef __ATmega_48A_PA_88A_PA_168A_PA_328_P

/*
 * Function: andlUART_init
 * Initializes UART.
 * -----------------------
 * ubrr: Value which will
 *       be load to UBRR
 *       registers.
 */
void andlUART_init(uint16_t ubrr)
{
	UBRR0H = (uint8_t)(ubrr >> 8); // ---\/
	UBRR0L = (uint8_t)ubrr; // --------> Set UBRR.
	UCSR0B |= (1 << RXEN0) | (1 << TXEN0); // Enable RX and TX.
	UCSR0C |= (1 << UCSZ00) | (1 << UCSZ01); // Set 8-bit data frame. None parity-bit and 1-bit stop-bit are enabled default.
}

/*
 * Function: andlUART_putchar
 * Put char via UART.
 * --------------------------
 * c: Char which will be send
 *    via UART.
 */
void andlUART_putchar(char c)
{
	while (!(UCSR0A & (1 << UDRE0))); // Wait when char can be send.
	UDR0 = c; // Send char.
}

/*
 * Function: andlUART_putstring
 * Put string via UART.
 * ----------------------------
 * str: String which will be
 *      send via UART.
 */
void andlUART_putstring(char *str)
{
	while (*str != '\0') // While current char of string isn't end-of-string char...
	{
		andlUART_putchar(*str++); // ...put this char and post-increment the pointer of string.
	}
}

/*
 * Function: andlUART_getchar
 * Get char from UART.
 * --------------------------
 * Returns: Char received from
 *          UART.
 */
char andlUART_getchar(void)
{
	while (!(UCSR0A & (1 << RXC0))); // Wait when char can be received.
	char c = UDR0; // Variable with received char.
	andlUART_putchar(c); // Put received char (echo).
	return c; // Return received char.
}

/*
 * Function: andlUART_getstring
 * Get string from UART.
 * ----------------------------
 * Returns: String received
 *          from UART.
 */
char *andlUART_getstring(void)
{
	char c; // Char received with getchar.
	char str[STR_LEN]; // Final string.
	uint8_t i = 0; // Help counter variable.

	do
	{
		str[i++] = c = andlUART_getchar(); // Receive char, put it into c and str variable and post-increment i variable...
	} while ((c != '\r') && (c != '\n')); // ...while c isn't carriage return or line feed symbol.

	if (strchr(str, '\r')) // If str contains carriage return symbol:
	{
		char *ptr = (char *)strchr(str, '\r'); // Pointer with carriage return symbol.
		*ptr = '\0'; // Put null symbol into ptr pointer.
	}

	if (strchr(str, '\n')) // If str contains line feed symbol:
	{
		char *ptr = (char *)strchr(str, '\n'); // Pointer with line feed symbol.
		*ptr = '\0'; // Put null symbol into ptr pointer.
	}

	andlUART_putstring("\r\n"); // Put carriage return and line feed symbols via UART.

	return str; // Return final string.
}

#endif
