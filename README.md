#
# andlUART 1.0.0

andlUART is a library may help you with initializing and using UART in ATmega microprocessors.
It was created by [andl](https://gitlab.com/.andl).

## Usage policy

andlUART is free and open-source, you can modify it.

## How do I compile it?

Instruction for compile andlUART is in release package, in `andlUART-instruction.pdf` file.

## Functions

For ATmega48A/PA/88A/PA/168A/PA/328/P:
- `void andlUART_init(uint16_t ubrr)`
    - Initializes UART.
    - ubrr: Value which will be loaded to UBRR registers.
- `void andlUART_putchar(char c)`
    - Put char via UART.
    - c: Char which will be send via UART.
- `void andlUART_putstring(char *str)`
    - Put string via UART.
    - str: String which will be send via UART.
- `char andlUART_getchar(void)`
    - Get char from UART.
    - Returns: Char received from UART.
- `char *andlUART_getstring(void)`
    - Get string from UART.
    - Returns: String received from UART.

After initialize UART you can put or get data using andlUART_putchar, andlUART_putstring, andlUART_getchar and andlUART_getstring functions.

Example using static library sending ”Hello World” string via UART every 5 seconds in ATmega328p:

```c
#include <util/delay.h>
#include <andlUART.h>

int main(void)
{
  andlUART_init(__UBRR); // Initialize UART. UBRR is calculated automatically using __UBRR macro.

  for (;;)
  {
    andlUART_putstring("Hello World!"); // Put "Hello World!" string via UART.
    _delay_ms(5000); // Wait 5 seconds.
  }
}
```
