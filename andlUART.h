/*
 * ========================================
 * [--------------andlUART.c--------------]
 * ========================================
 * This source file has definitions of functions which can initialize and use UART.
 * It can be used for compile static library or direct include to Your project.
 *
 * Created by andl on 30.03.2019
 */

// -------------INCLUDE GUARD--------------
#ifndef ANDLUART_H
#define ANDLUART_H

// ----------------INCLUDES----------------
#include <avr/io.h>

// -------------CONFIGURATION--------------
#define BAUD 9600 // Change to baud you use
#define STR_LEN 32 // Change to max length of received string

#define __UBRR F_CPU/16/BAUD-1 // Use this macro to calculate UBRR automatically
#define __ATmega_48A_PA_88A_PA_168A_PA_328_P (defined __AVR_ATmega48A__) || (defined __AVR_ATmega48PA__) || (defined __AVR_ATmega88A__) || (defined __AVR_ATmega88PA__) || (defined __AVR_ATmega168A__) || (defined __AVR_ATmega168PA__) || (defined __AVR_ATmega328__) || (defined __AVR_ATmega328P__)

// ---------------FUNCTIONS----------------

/*
 * Function: andlUART_init
 * Initializes UART.
 * -----------------------
 * ubrr: Value which will
 *       be load to UBRR
 *       registers.
 */
void andlUART_init(uint16_t ubrr);

/*
 * Function: andlUART_putchar
 * Put char via UART.
 * --------------------------
 * c: Char which will be send
 *    via UART.
 */
void andlUART_putchar(char c);

/*
 * Function: andlUART_putstring
 * Put string via UART.
 * ----------------------------
 * str: String which will be
 *      send via UART.
 */
void andlUART_putstring(char *str);

/*
 * Function: andlUART_getchar
 * Get char from UART.
 * --------------------------
 * Returns: Char received from
 *          UART.
 */
char andlUART_getchar(void);

/*
 * Function: andlUART_getstring
 * Get string from UART.
 * ----------------------------
 * Returns: String received
 *          from UART.
 */
char *andlUART_getstring(void);

#endif // ANDLUART_H
